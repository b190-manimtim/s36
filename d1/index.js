// Basic imports
const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes")

// server setup
const app = express();
const port = 3000;

// mongoDB connection
mongoose.connect("mongodb+srv://chanomanimtim:12455421@wdc028-course-booking.ivug0tv.mongodb.net/b190-to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
)
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the database"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Allows all the task routes created in the taskRoutes.js files to use "/tasks" route
app.use("/tasks", taskRoutes)

// server running confirmation
app.listen(port, ()=> console.log(`Server running at port: ${port}`))