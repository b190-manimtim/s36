// contains functions and business logic of our express js application
// all operations it can do will be place in this file.
// allows us to use the contents of "task.js" file in the "model" folder

const Task = require("../models/task")

// defines the function to be used in the "taskRoutes.js"
module.exports.getAllTasks=() => {
    return Task.find({}).then(result=>{
        return result
    })
}

// function for creating a task

module.exports.createTask = (requestBody) => {
    let newTask = new Task({
        name: requestBody.name
    })
    return newTask.save().then((task, error) => {
        if (error){
            console.log(error);
            return false   
        } else {
            return task
        }
    })
}

// function for deleting a task
module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((result,error) => {
        if(error){
            console.log(error);
            return false;
        } else{
            return result
        }
    })
}

// function for updating a task
module.exports.updateTask = (taskId, newContent) => {
    return Task.findById(taskId).then((result, error)=>{
        if(error){
            console.log(error);
            return false;
        } else{
            result.name = newContent.name
            return result.save().then((updatedTask, error)=>{
                if(error){
                    console.log(error)
                    return false
                } else{
                    return updatedTask
                }
            })
        }
    })
}

// search specific task ----------- ACTIVITY
module.exports.getSpecificTask=(taskId) => {
    return Task.findById(taskId).then((result, error)=>{
        if(error){
            console.log(error);
            return false;
        } else{
            return result
        }
    })
}

// Update status of task to complete ----------- ACTIVITY
module.exports.updateStatus = (taskId, newContent) => {
    return Task.findById(taskId).then((result, error)=>{
        if(error){
            console.log(error);
            return false;
        } else{
            result.status = "complete"
            return result.save().then((updatedStatus, error)=>{
                if(error){
                    console.log(error)
                   // return false
                } else{
                    return updatedStatus
                }
            })
        }
    })
}