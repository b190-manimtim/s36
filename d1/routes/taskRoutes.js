// contains all endpoints for our application
// app.js/index.js should only be concerned with information about the server
// we have to use the Router method inside express
const express = require("express");

// Allows us to access HTTP method middlewares that makes it easier to create routing system
const router = express.Router();

// this allows us to use the contents of taskControllers.js in "controllers" folder
const taskController = require("../controller/taskController")

router.get("/",(req,res) => {
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

// route for adding task
router.post("/",(req,res) => {
    taskController.createTask(req.body).then(result => res.send(result))
})

// route for deleting task
router.delete("/:id", (req,res)=>{
    taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

// route for updating task
router.put("/:id",(req,res)=>{
    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

// search specific task ----------- ACTIVITY
router.get("/:id",(req,res) => {
    taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

// Update status of task to complete ----------- ACTIVITY
router.put("/:id/complete",(req,res)=>{
    taskController.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})


// modules.export - Allows us to export the file where it is inserted to be used by other files such as app.js/index.js
module.exports = router;

